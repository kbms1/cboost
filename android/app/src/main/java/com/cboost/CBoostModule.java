package com.cboost;

import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.CBLocation;
import com.chartboost.sdk.ChartboostDelegate;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Promise;

import java.util.HashMap;
import java.util.Map;

public class CBoostModule extends ReactContextBaseJavaModule {

    private String location = "default";

    public CBoostModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "RNCBoost";
    }

    @Override
    public Map<String, Object> getConstants() {
        final Map<String, Object> constants = new HashMap<>();
        constants.put("DEV_1", "Boris");
        constants.put("DEV_2", "Matt");
        return constants;
    }

//    @ReactMethod
//    public void show(String message, int duration) {
//        Toast.makeText(getReactApplicationContext(), message, duration).show();
//    }

    @ReactMethod
    public void getSdkVersion(Promise promise) {
        promise.resolve(Chartboost.getSDKVersion());
    }

    @ReactMethod
    public void showInterstitial() {
        Chartboost.showInterstitial(location);
    }

    @ReactMethod
    public void hasInterstitial(Promise promise) {
        promise.resolve(Chartboost.hasInterstitial(location));
    }

    @ReactMethod
    public void cacheInterstitial(Promise promise) {
        Chartboost.cacheInterstitial(location);
        promise.resolve("done caching");
    }
}
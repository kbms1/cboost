/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  NativeModules,
  TouchableOpacity
} from 'react-native';

const Toast = NativeModules.RNToast;
const CBoost = NativeModules.RNCBoost;
const NatMods = NativeModules;

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class App extends Component {
  render() {
    console.log(">>>>> Toast: ", Toast);
    console.log(">>>>> CBoost: ", CBoost);
    CBoost.getSdkVersion().then(version => console.log(">>>>> CBoost Sdk version: ", version));
    
    CBoost.hasInterstitial().then(interstitial => {
      console.log(">>>>> hasInterstitial?: ", interstitial);
      if (!interstitial) {
        CBoost.cacheInterstitial()
          .then(() => CBoost.hasInterstitial().then(interstitial => console.log(">>>> Now hasInterstitial?: ", interstitial)));
      }
    });
    
    console.log(">>>>> NatMods: " + JSON.stringify(NatMods));
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to React Native!
        </Text>
        <Text style={styles.instructions}>
          To get started, edit App.js
        </Text>
        <Text style={styles.instructions}>
          {instructions}
        </Text>
        <TouchableOpacity onPress={() => CBoost.showInterstitial()} style={{ backgroundColor: "red" }}>
          <Text>Show Interstitial</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

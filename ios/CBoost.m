

#import <Foundation/Foundation.h>
#import <Chartboost/Chartboost.h>

#import "CBoost.h"


@implementation CBoost

RCT_EXPORT_MODULE(RNCBoost);

RCT_EXPORT_METHOD(getSdkVersion:
                  (RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject)
{
  resolve([Chartboost getSDKVersion]);
}

RCT_EXPORT_METHOD(showInterstitial)
{
  [Chartboost showInterstitial:CBLocationHomeScreen];
}

RCT_EXPORT_METHOD(hasInterstitial:
                  (RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject)
{
  resolve(@([Chartboost hasInterstitial:CBLocationHomeScreen]));
}

RCT_EXPORT_METHOD(cacheInterstitial:
                  (RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject)
{
  [Chartboost cacheInterstitial:CBLocationHomeScreen];
  resolve(@"done caching");
}


@end
